//
//  ViewController.swift
//  Pokedex-2018
//
//  Created by Christian on 13/6/18.
//  Copyright © 2018 Christian. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var name1: String = ""
    
    var peso1: Int = 0
    
    var altura1: Int = 0
    
    var id1: Int = 0
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pokemon.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
            let cell = UITableViewCell()
            cell.textLabel?.text = pokemon[indexPath.row].name
        peso1 = pokemon[indexPath.row].height
        altura1 = pokemon[indexPath.row].weight
        name1 = pokemon[indexPath.row].name
        id1 = pokemon[indexPath.row].id
            return cell
        

    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "info"{
            let vistapoke = segue.destination
            as? PokedexViewController
            //vistapoke?.name
            vistapoke?.peso = peso1
            vistapoke?.altura = altura1
            vistapoke?.name2 = name1
            vistapoke?.id = id1
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    var pokemon:[Pokemon] = []
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let network = Network()
        network.getAllPokemon { (pokemonArray) in
            self.pokemon = pokemonArray
            self.pokemonTableView.reloadData()
        }
            
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    //MARK:- TableView
    
    
  
    @IBOutlet weak var pokemonTableView: UITableView!
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
            performSegue(withIdentifier: "info", sender: self)
        
    }
    

}


