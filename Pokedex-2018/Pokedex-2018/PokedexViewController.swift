//
//  PokedexViewController.swift
//  Pokedex-2018
//
//  Created by Christian on 20/6/18.
//  Copyright © 2018 Christian. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

class PokedexViewController: UIViewController {
    
    var name2 : String = ""
    var peso : Int = 0
    var altura : Int = 0
    var id : Int = 0
    
    @IBOutlet weak var image: UIImageView!
    
    
    @IBOutlet weak var name: UILabel!
    
    
    @IBOutlet weak var weight: UILabel!
    
    @IBOutlet weak var height: UILabel!
    
    
    @IBOutlet weak var idPoke: UILabel!
    
    
    
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        //weight.text = String(peso)
        //height.text = String(altura)
        
        //weight.text = String(peso)
        //height.text = String(altura)
        //name.text = String(name2)
        //idPoke.text = String(id)
        
        self.mostrarDatos()


        // Do any additional setup after loading the view.
    }
    
    func mostrarDatos() {
        weight.text = String(peso)
        height.text = String(altura)
        name.text = String(name2)
        idPoke.text = String(id)
        
        let url = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/\(id).png"
        
        
        Alamofire.request(url).responseImage { response in
            if let image = response.result.value {
                
                self.image.image = image
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    
    
    

}
